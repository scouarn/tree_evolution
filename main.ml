#require "graphics"
open Graphics

(* Ref: The Algorithmic Beauty Of Plants
    by Przemyslaw Prusinkiewicz and Aristid Lindenmayer
*)

let rad_of_deg x = Float.pi *. x /. 180.0
let explode s = List.init (String.length s) (String.get s)


type state = {
    x:   float;
    y:   float;
    a:   float;
    pop: state;
}

type axiom = Init | Sleep | Grow of float | Rot of float | Push | Pop
type rules = axiom -> axiom list


let render_axiom (s: state) = function
    | Grow dist ->
        (* let a  = s.a +. 0.1 *. ((Random.float Float.pi) -. Float.pi /. 2.0) in *)
        let a = s.a in
        let x' = s.x +. dist *. cos a
        and y' = s.y +. dist *. sin a in
        lineto (int_of_float x') (int_of_float y');
        { s with x = x'; y = y' }

    | Rot a -> { s with a = s.a +. a }
    | Push  -> { s with pop = s }
    | Pop   ->
        moveto (int_of_float s.pop.x) (int_of_float s.pop.y);
        s.pop

    | _   -> s


let expand_axioms (rules: rules) axioms =
    List.fold_left
        (fun acc axiom -> acc @ rules axiom)
        [] axioms

let rec expand (n: int) (rules: rules) =
    if n = 0 then [Init] else
    expand_axioms rules (expand (n-1) rules)


let render_axioms (axioms: axiom list) : unit =
    open_graph "";
    set_window_title "Render";
    clear_graph ();

    let x =  (size_x () / 2)
    and y =  0 in
    moveto x y;

    let rec init_state : state = {
        x   = float_of_int x;
        y   = float_of_int y;
        a   = Float.pi /. 2.0;
        pop = init_state;
    } in

    let _ = List.fold_left
        (fun state axiom -> render_axiom state axiom)
        init_state
        axioms
    in

    try
        ignore (wait_next_event [Key_pressed]);
        close_graph ();
    with Graphic_failure s -> print_endline s


(* Expand then render *)
let render_rules (n: int) (rules: rules) : unit =
    render_axioms (expand n rules)

(* Animated expansion *)
let animate (n: int) (rules: rules) : unit =
    open_graph "";
    set_window_title "Render";

    let x =  (size_x () / 2)
    and y =  0 in

    let rec init_state : state = {
        x   = float_of_int x;
        y   = float_of_int y;
        a   = Float.pi /. 2.0;
        pop = init_state;
    } in


    let axioms = ref [Init] in

    for i = 1 to n do
        axioms := expand_axioms rules !axioms;

        clear_graph ();
        moveto x y;

        let _ = List.fold_left
            (fun state axiom -> render_axiom state axiom)
            init_state
            !axioms
        in

        Unix.sleepf 0.5;
    done;

    try
        ignore (wait_next_event [Key_pressed]);
        close_graph ()
    with Graphic_failure s -> print_endline s


let tree_a d =
    let a = rad_of_deg 25.7 in
    function
    | Init -> [ Grow d ]
    | Grow _ -> [ (* F[+F]F[-F]F *)
        Grow d; Push; Rot a; Grow d; Pop;
        Grow d; Push; Rot ~-.a; Grow d; Pop; Grow d;
    ]
    | term -> [ term ]

let tree_b d =
    let a = rad_of_deg 20.0 in
    function
    | Init -> [ Grow d ]
    | Grow _ -> [ (* F[+F]F[-F][F] *)
        Grow d;
        Push; Rot a; Grow d; Pop; Grow d;
        Push; Rot ~-.a; Grow d; Pop;
        Push; Grow d; Pop
    ]
    | term -> [ term ]

let tree_c d =
    let a = rad_of_deg 22.5 in
    function
    | Init -> [ Grow d ]
    | Grow _ -> [ (* FF-[-F+F+F]+[+F-F-F] *)
        Grow d; Grow d; Rot ~-.a;
        Push; Rot ~-.a; Grow d; Rot a; Grow d; Rot a; Grow d; Pop; Rot a;
        Push; Rot a; Grow d; Rot ~-.a; Grow d; Rot ~-.a; Grow d; Pop;
    ]
    | term -> [ term ]

let tree_d d =
    let a = rad_of_deg 20.0 in
    function
    | Init -> [ Sleep ]
    | Sleep -> [ (* F[+X]F[-X]+X *)
        Grow d; Push; Rot a; Sleep; Pop;
        Grow d; Push; Rot ~-.a; Sleep; Pop;
        Rot a; Sleep
    ]
    | Grow _ -> [ Grow d; Grow d ]
    | term -> [ term ]

let tree_e d =
    let a = rad_of_deg 25.7 in
    function
    | Init -> [ Sleep ]
    | Sleep -> [ (* F[+X][-X]FX *)
        Grow d; Push; Rot a; Sleep; Pop;
        Push; Rot ~-.a; Sleep; Pop;
        Grow d; Sleep
    ]
    | Grow _ -> [ Grow d; Grow d ]
    | term -> [ term ]

let tree_f d =
    let a = rad_of_deg 22.5 in
    function
    | Init -> [ Sleep ];
    | Sleep -> [ (* F-[[X]+X]+F[+FX]-X *)
        Grow d; Rot ~-.a;
        Push; Push; Sleep; Pop; Rot a; Sleep; Pop;
        Rot a; Grow d;
        Push; Rot a; Grow d; Sleep; Pop;
        Rot ~-.a; Sleep
    ]
    | Grow _ -> [ Grow d; Grow d ]
    | term -> [ term ];
