#!/usr/bin/env ocaml
#use "topfind"
#require "graphics"
open Graphics

let rad_of_deg x = Float.pi *. x /. 180.0
let explode s = List.init (String.length s) (String.get s)


type state = {
    x:   float;
    y:   float;
    d:   float;
    a:   float;
    da:  float;
    pop: state; (* State stack *)
}

type rules = {
    angle:  float;
    expand: char -> string;
}


(* Draw and return next state *)
let render (s: state) (sym: char) : state =
    match sym with
    | 'F' ->
        let x' = s.x +. s.d *. cos s.a      (* Compute new position *)
        and y' = s.y +. s.d *. sin s.a in
        lineto (int_of_float x') (int_of_float y');
        { s with x = x'; y = y' }

    | '-' -> { s with a = s.a -. s.da }
    | '+' -> { s with a = s.a +. s.da }
    | '[' -> { s with pop = s }
    | ']' ->
        moveto (int_of_float s.pop.x) (int_of_float s.pop.y);
        s.pop

    | _   -> s


(* Apply rules on each symbol *)
let expand_syms (rules: rules) (syms : string) : string =
    String.fold_left (fun acc atom -> acc ^ rules.expand atom) "" syms


(* Apply rules n times *)
let rec expand_rules (n: int) (rules: rules) =
    if n = 0 then "o" else
    expand_syms rules (expand_rules (n-1) rules)


(* Render string of symbols *)
let render_syms (dist: float) (angle: float) (syms: string) : unit =

    open_graph "";
    set_window_title "Render";
    clear_graph ();

    let x =  (size_x () / 2)
    and y =  0 in
    moveto x y;

    let rec init_state : state = {
        x   = float_of_int x;
        y   = float_of_int y;
        d   = dist;
        a   = Float.pi /. 2.0;
        da  = angle;
        pop = init_state;
    } in

    (* For each element in *)
    let _ = String.fold_left
        (fun state sym -> render state sym)
        init_state
        syms
    in


    try
        ignore (wait_next_event [Key_pressed]);
        close_graph ();
    with Graphic_failure s -> print_endline s


(* Expand n times and render *)
let render_rules (dist: float) (n: int) (rules: rules) : unit =
    render_syms dist rules.angle (expand_rules n rules)



let tree_a = {
    angle = rad_of_deg 25.7;
    expand = function
        | 'o' -> "F"
        | 'F' -> "F[+F]F[-F]F"
        | term -> String.make 1 term
}

let tree_b = {
    angle = rad_of_deg 20.0;
    expand = function
        | 'o' -> "F"
        | 'F' -> "F[+F]F[-F][F]"
        | term -> String.make 1 term
}

let tree_c = {
    angle = rad_of_deg 22.5;
    expand = function
        | 'o' -> "F"
        | 'F' -> "FF-[-F+F+F]+[+F-F-F]"
        | term -> String.make 1 term
}

let tree_d = {
    angle = rad_of_deg 20.0;
    expand = function
        | 'o' -> "F"
        | 'X' -> "F[+X]F[-X]+X"
        | 'F' -> "FF"
        | term -> String.make 1 term
}

let tree_e = {
    angle = rad_of_deg 25.7;
    expand = function
        | 'o' -> "F"
        | 'X' -> "F[+X][-X]FX"
        | 'F' -> "FF"
        | term -> String.make 1 term
}

let tree_f = {
    angle = rad_of_deg 22.5;
    expand = function
        | 'o' -> "X"
        | 'X' -> "F-[[X]+X]+F[+FX]-X"
        | 'F' -> "FF"
        | term -> String.make 1 term
}

let plant = {
    angle = rad_of_deg 22.5;
    expand = function
        | 'o' -> "R"
        | 'R' -> "FF[+FL]F[-FL]R"
        | 'L' -> "-F+F+F++++++F+F+F"
        | term -> String.make 1 term
}


let () = render_rules 20.0 8 plant
